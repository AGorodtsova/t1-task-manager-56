package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataXmlSaveJaxBRequest;

@Component
public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data in xml file";

    @NotNull
    private final String NAME = "data-save-xml-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(getToken());
        domainEndpoint.saveDataXmlJaxb(request);
    }

}
