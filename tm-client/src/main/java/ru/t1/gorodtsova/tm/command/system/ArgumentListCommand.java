package ru.t1.gorodtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.model.ICommand;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-arg";

    @NotNull
    private final String DESCRIPTION = "Show argument list";

    @NotNull
    private final String NAME = "arguments";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@Nullable final ICommand command : commands) {
            if (command == null) continue;
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
