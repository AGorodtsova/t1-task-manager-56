package ru.t1.gorodtsova.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.user.UserLoginRequest;
import ru.t1.gorodtsova.tm.dto.response.user.UserLoginResponse;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

@Component
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "User login";

    @NotNull
    private final String NAME = "login";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse response = authEndpoint.login(request);
        if (!response.getSuccess()) throw new Exception(response.getMessage());
        @Nullable final String token = response.getToken();
        setToken(token);
        System.out.println(token);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
