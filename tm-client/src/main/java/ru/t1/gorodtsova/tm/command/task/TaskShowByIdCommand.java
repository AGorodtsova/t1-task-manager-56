package ru.t1.gorodtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.model.TaskDTO;
import ru.t1.gorodtsova.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Display task by id";

    @NotNull
    private final String NAME = "task-show-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken(), id);
        @Nullable final TaskDTO task = taskEndpoint.showTaskById(request).getTask();
        showTask(task);
    }

}
