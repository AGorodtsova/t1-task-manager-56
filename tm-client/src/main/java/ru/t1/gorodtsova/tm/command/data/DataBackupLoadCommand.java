package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBackupLoadRequest;

@Component
public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load backup from file";

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        domainEndpoint.loadDataBackup(request);
    }

}
