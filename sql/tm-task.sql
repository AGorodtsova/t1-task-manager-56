-- Table: public.tm_task

-- DROP TABLE IF EXISTS public.tm_task;

CREATE TABLE IF NOT EXISTS public.tm_task
(
    id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    created time without time zone NOT NULL,
    name character varying(200) COLLATE pg_catalog."default" NOT NULL,
    descrptn character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    status character varying(50) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(100) COLLATE pg_catalog."default",
    project_id character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT tm_task_pk PRIMARY KEY (id),
    CONSTRAINT project_id_fk FOREIGN KEY (project_id)
        REFERENCES public.tm-project (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.tm_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_task
    OWNER to postgres;

COMMENT ON TABLE public.tm_task
    IS 'TASK MANAGER TASK TABLE';