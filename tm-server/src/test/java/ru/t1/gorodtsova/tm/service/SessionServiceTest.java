/*package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.api.service.dto.ISessionDtoService;
import ru.t1.gorodtsova.tm.api.service.dto.IUserDtoService;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.service.dto.SessionDtoService;
import ru.t1.gorodtsova.tm.service.dto.UserDtoService;

import static ru.t1.gorodtsova.tm.constant.SessionTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final ISessionDtoService sessionService = new SessionDtoService(connectionService);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        userService.add(USER_LIST);
        sessionService.add(SESSION_LIST);
    }

    @After
    public void tearDown() {
        sessionService.removeAll();
        userService.removeAll();
    }

    @Test
    public void add() {
        sessionService.removeAll();
        sessionService.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getUserId(), sessionService.findAll().get(0).getUserId());
    }

    @Test
    public void addByUser() {
        sessionService.removeAll();
        sessionService.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findAll().get(0).getId());
        Assert.assertEquals(USER1.getId(), sessionService.findAll().get(0).getUserId());
        Assert.assertNull(USER1.getId(), null);
        thrown.expect(UserIdEmptyException.class);
        sessionService.add(null, USER1_SESSION2);
    }

    @Test
    public void set() {
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.set(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(SESSION_LIST.size(), sessionService.findAll().size());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.findAll(USER1.getId()).size());
        Assert.assertNotEquals(USER1_SESSION_LIST.size(), sessionService.findAll(USER2.getId()).size());
        thrown.expect(UserIdEmptyException.class);
        sessionService.findAll("");
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findOneById(USER1.getId(), USER1_SESSION1.getId()).getId());

        thrown.expect(ModelNotFoundException.class);
        sessionService.findOneById(USER1.getId(), USER2_SESSION1.getId());

        thrown.expect(UserIdEmptyException.class);
        sessionService.findOneById(null, USER1_SESSION1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        sessionService.findOneById(USER1.getId(), null);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.removeAll();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertFalse(sessionService.findAll(USER1.getId()).isEmpty());
        sessionService.removeAll(USER1.getId());
        Assert.assertTrue(sessionService.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        sessionService.removeAll("");
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertTrue(sessionService.existsById(USER1_SESSION2.getId()));
        sessionService.removeOneById(USER1.getId(), USER1_SESSION2.getId());
        Assert.assertFalse(sessionService.existsById(USER1_SESSION2.getId()));

        thrown.expect(ModelNotFoundException.class);
        sessionService.removeOneById(USER2.getId(), USER1_SESSION1.getId());

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOneById(null, USER1_SESSION1.getId());

        thrown.expect(IdEmptyException.class);
        sessionService.removeOneById(USER1.getId(), null);
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionService.existsById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2.getId(), USER1_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.existsById(null, USER1_SESSION1.getId());

        thrown.expect(IdEmptyException.class);
        sessionService.existsById(USER1.getId(), null);
    }

}
*/