package ru.t1.gorodtsova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.t1.gorodtsova.tm.api.repository.dto.IDtoRepository;
import ru.t1.gorodtsova.tm.api.service.dto.IDtoService;
import ru.t1.gorodtsova.tm.dto.model.AbstractModelDTO;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    public EntityManager getEntityManager() {
        return getRepository().getEntityManager();
    }

    @NotNull
    protected abstract IDtoRepository<M> getRepository();

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            models.forEach(repository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.set(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @Nullable final M model = repository.findOneById(id);
            if (model == null) throw new EntityNotFoundException();
            return model;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(collection);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOne(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model;
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            model = repository.findOneById(id);
            if (model == null) throw new EntityNotFoundException();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            return repository.findOneById(id) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final IDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

}
