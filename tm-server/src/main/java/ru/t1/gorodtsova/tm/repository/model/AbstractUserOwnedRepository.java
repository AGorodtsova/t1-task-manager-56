package ru.t1.gorodtsova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;
import ru.t1.gorodtsova.tm.model.User;

public abstract class AbstractUserOwnedRepository
        <M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUser(entityManager.find(User.class, userId));
        return add(model);
    }

    @Override
    public void removeOne(@NotNull final String userId, @NotNull final M model) {
        if (existsById(userId, model.getId())) entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
    }

}
