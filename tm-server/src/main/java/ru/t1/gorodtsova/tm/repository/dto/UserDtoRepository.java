package ru.t1.gorodtsova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.gorodtsova.tm.dto.model.UserDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDTO m";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull final String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM UserDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM UserDTO m";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setMaxResults(1)
                .getFirstResult();
    }

}
