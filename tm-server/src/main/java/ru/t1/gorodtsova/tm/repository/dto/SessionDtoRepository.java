package ru.t1.gorodtsova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    @Override
    @NotNull
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class).getResultList();
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    @Nullable
    public SessionDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM SessionDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

}
