package ru.t1.gorodtsova.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;
import ru.t1.gorodtsova.tm.dto.model.TaskDTO;
import ru.t1.gorodtsova.tm.dto.model.UserDTO;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.Session;
import ru.t1.gorodtsova.tm.model.Task;
import ru.t1.gorodtsova.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@ComponentScan("ru.t1.gorodtsova.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, propertyService.getDatabaseDriver());
        settings.put(URL, propertyService.getDatabaseUrl());
        settings.put(USER, propertyService.getDatabaseUser());
        settings.put(PASS, propertyService.getDatabasePassword());
        settings.put(DIALECT, propertyService.getDatabaseDialect());
        settings.put(HBM2DDL_AUTO, propertyService.getDatabaseDdlAuto());
        settings.put(SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(FORMAT_SQL, propertyService.getDatabaseShowSql());
        settings.put(USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseSecondLevelCash());
        settings.put(CACHE_REGION_FACTORY, propertyService.getDatabaseFactoryClass());
        settings.put(USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCash());
        settings.put(USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinPuts());
        settings.put(CACHE_REGION_PREFIX, propertyService.getDatabaseRegionPrefix());
        settings.put(CACHE_PROVIDER_CONFIG, propertyService.getDatabaseConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(Session.class);
        source.addAnnotatedClass(SessionDTO.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
