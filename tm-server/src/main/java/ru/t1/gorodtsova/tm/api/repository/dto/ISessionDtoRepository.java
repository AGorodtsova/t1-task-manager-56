package ru.t1.gorodtsova.tm.api.repository.dto;

import ru.t1.gorodtsova.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {
}
