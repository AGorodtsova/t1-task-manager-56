package ru.t1.gorodtsova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    void update(@NotNull M model);

    void removeAll();

    void removeAll(@NotNull Collection<M> collection);

    void removeOne(@NotNull M model);

    void removeOneById(@NotNull String id);

    boolean existsById(@NotNull String id);

    int getSize();

}
