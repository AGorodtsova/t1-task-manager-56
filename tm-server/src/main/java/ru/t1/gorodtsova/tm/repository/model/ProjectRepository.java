package ru.t1.gorodtsova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.api.repository.model.IProjectRepository;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final String jpql = "SELECT m FROM Project";
        return entityManager.createQuery(jpql, Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId ORDER BY :sort";
        return entityManager
                .createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public void removeAll() {
        @NotNull final List<Project> projects = findAll();
        if (projects.isEmpty()) return;
        removeAll(projects);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAll(userId);
        if (projects.isEmpty()) return;
        removeAll(projects);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project m";
        return entityManager.createQuery(jpql, Project.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

}
